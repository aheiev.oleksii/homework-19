const list = [12, 22, 221, 33, 5, 2];

function copy(list, func = null) {
  const copyArr = [];
  for (let i = 0; i < list.length; i++) {
    if (typeof func === "function") {
      copyArr.push(func(list[i]));
    } else {
      copyArr.push(list[i]);
    }
  }
  return copyArr;
}

const newList = copy(list, function (value) {
  return value * 10;
});

console.log(newList);
